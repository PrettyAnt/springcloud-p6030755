package com.gientech.redisdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class RedisDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(RedisDemoApplication.class, args);
        System.out.println("Hello redis Exercise!");
    }

//	@Bean
//	public RedisTemplate redisTemplatez() {
//		return new RedisTemplate();
//	}

}
