package com.gientech.redisdemo.controller;

import io.netty.util.internal.StringUtil;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.concurrent.TimeUnit;

@RestController
public class RedisDemoController {
    //1、基于springbot项目，启动两个实例，使用实例1 去往redis里生成key value值
    //使用实例2.获取key值，并展示在页面上
    //2、基于springboot项目，启动两个实例，实现分布式。1、体现竞争 2、持有锁，超时释放
    @Resource
    StringRedisTemplate stringRedisTemplate;
    @GetMapping("redis/setvalue")
    public String setValue() {
        String key = "PrettyAnt";
        String value= "28";
//        stringRedisTemplate.opsForValue().set(key,value);
        stringRedisTemplate.opsForValue().set(key,value,20,TimeUnit.SECONDS);
        return "set success";
    }

    @GetMapping("redis/getvalue")
    public String getValue() {
        String key = "PrettyAnt";
        String str = stringRedisTemplate.opsForValue().get(key);
        if (StringUtil.isNullOrEmpty(str)) {
            return "没有这个key";
        }

        Long expire = stringRedisTemplate.getExpire(key, TimeUnit.SECONDS);
        return "--->>>" + expire;
    }
}
