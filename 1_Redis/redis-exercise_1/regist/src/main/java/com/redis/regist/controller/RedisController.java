package com.redis.regist.controller;

import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.concurrent.TimeUnit;

@RestController
public class RedisController {
    @Resource
    StringRedisTemplate stringRedisTemplate;

    @GetMapping("redis/regist")
    public String regist() {

        String key = "userName";
        String value = "PrettyAnt";
        if (Boolean.TRUE.equals(stringRedisTemplate.hasKey(key))) {
            return "已经注册,请勿重复注册";
        }
        stringRedisTemplate.opsForValue().set(key,value,20, TimeUnit.SECONDS);

        return "注册成功";
    }
}
