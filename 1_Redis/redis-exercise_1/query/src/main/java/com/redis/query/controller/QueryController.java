package com.redis.query.controller;

import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
public class QueryController {
    @Resource
    StringRedisTemplate stringRedisTemplate;

    @GetMapping("redis/query")
    public String query() {

        String key = "userName";
        if (!Boolean.TRUE.equals(stringRedisTemplate.hasKey(key))) {
            return "用户信息已过期";
        }
        String value = stringRedisTemplate.opsForValue().get(key);

        return "查询结果:"+value;
    }
}
