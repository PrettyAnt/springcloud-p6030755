package com.redis.distribute_1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Distribute1Application {

	public static void main(String[] args) {
		SpringApplication.run(Distribute1Application.class, args);
	}

}
