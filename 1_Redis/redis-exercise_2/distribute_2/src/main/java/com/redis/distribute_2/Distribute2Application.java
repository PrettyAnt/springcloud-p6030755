package com.redis.distribute_2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Distribute2Application {

	public static void main(String[] args) {
		SpringApplication.run(Distribute2Application.class, args);
	}

}
