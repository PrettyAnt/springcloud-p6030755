package com.redis.distribute_2.controller;

import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.concurrent.TimeUnit;

@RestController
public class Person2 {
    @Resource
    StringRedisTemplate stringRedisTemplate;

    String key1 = "userKey01";
    String user1 = "userValue01";
    String key2 = "userKey02";
    String user2 = "userValue02";

    @GetMapping("/get")
    public String get() {
        Boolean ifAbsent = stringRedisTemplate.opsForValue().setIfAbsent(key1, user1);
        if (ifAbsent) {
            return "竞争成功";
        } else {
            return "竞争失败";
        }
    }


    @GetMapping("/time")
    public String isLock() {
        stringRedisTemplate.opsForValue().setIfAbsent(key2, user2, 10, TimeUnit.SECONDS);
        String currentValue = stringRedisTemplate.opsForValue().get(key2);
        if (currentValue.equals(user2)) {
            return "正在占用";
        } else {
            return "等待使用";
        }
    }

}
