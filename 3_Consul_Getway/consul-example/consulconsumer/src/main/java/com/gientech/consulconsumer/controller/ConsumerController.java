package com.gientech.consulconsumer.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import java.net.URI;
import java.util.List;

@RestController
public class ConsumerController {
    @Resource
    private RestTemplate restTemplate;
    @Autowired
    DiscoveryClient discoveryClient;

    @GetMapping("/consul-consumer")
    public String consulConsumer() {
        List<ServiceInstance> instances;
        String targetUrl = "";
        String template = "";
        try {
            //通过consumer去访问provider
//            template = restTemplate.getForObject("http://localhost:8020/consul-provider", String.class);
//            List<String> services = this.discoveryClient.getServices();
//            for (int i = 0; i < services.size(); i++) {
//                String service = services.get(i);
//                System.out.println("services --->>> "+service);
//            }
            instances = this.discoveryClient.getInstances("consul-provider02");
            ServiceInstance serviceInstance = instances.get(0);
            URI uri = serviceInstance.getUri();
            targetUrl = uri + "/consul-provider";
            System.out.println("targetUrl: " + targetUrl);
            template = restTemplate.getForObject(targetUrl, String.class);

        } catch (Exception e) {
            return "异常:" + e.getMessage();
        }

        return "consul-consumer: " + template;
    }

    @RequestMapping("/logintest")
    public String loginTest() {
        return "login . . .";
    }
}
