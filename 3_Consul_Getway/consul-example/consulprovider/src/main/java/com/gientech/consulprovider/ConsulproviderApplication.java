package com.gientech.consulprovider;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@SpringBootApplication(exclude= {DataSourceAutoConfiguration.class})
public class ConsulproviderApplication {

	public static void main(String[] args) {
		SpringApplication.run(ConsulproviderApplication.class, args);
	}

}
