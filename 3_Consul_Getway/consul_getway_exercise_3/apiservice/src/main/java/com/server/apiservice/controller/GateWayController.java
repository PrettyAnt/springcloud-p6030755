package com.server.apiservice.controller;


import com.server.apiservice.domain.ServiceInfo;
import org.springframework.data.redis.core.ListOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import java.net.URI;
import java.net.http.HttpRequest;
import java.util.List;

@RestController
public class GateWayController {
    @Resource
    RedisTemplate redisTemplate;

    @Resource
    RestTemplate restTemplate;

    /**
     * 服务A: http://localhost:8020/consul-provider 服务是开启的。
     * 服务B:
     *  将name ip port 存储在redis中，其中key为services
     *  通过serviceName去redis中去匹配name,获取服务信息
     *  serviceName  获取-->ip、port (结果形如:192.168.134.62:8035)
     *  serviceInterface 即目标服务A的接口名consul-provider
     *  最终结果为:B服务+A服务存储在redis的信息+A服务的接口名
     *             --->>> http://192.168.134.62:8035/A服务存储在redis中的名称/A服务的接口名
     *             --->>> http://192.168.134.62:8035/provider-test/consul-provider
     *
     * @param serviceName
     * @param serviceInterface
     * @return
     */
    @RequestMapping("{serviceName}/{serviceInterface}")
    public String serviceGateway(@PathVariable("serviceName") String serviceName,
                                 @PathVariable("serviceInterface") String serviceInterface) {
        ListOperations<String, ServiceInfo> ops = redisTemplate.opsForList();
        List<ServiceInfo> services = ops.range("services", 0, -1);
        // 根据请求的serviceName获取对应的服务信息
        ServiceInfo serviceInfo = services.stream().filter(info -> info.getName().equals(serviceName))
                .findFirst().get();
        System.out.println("servceInfo: "+serviceInfo);
        if (serviceInfo != null) {
            String url = "http://" + serviceInfo.getIp() + ":" + serviceInfo.getPort() + "/" + serviceInterface;
            System.out.println("url  ---->>  "+url);
           return restTemplate.getForObject(url, String.class);
        } else {
            return serviceName + "服务未注册,请注册后重试";
        }
    }
}
