package com.server.apiservice.controller;

import com.server.apiservice.domain.ServiceInfo;
import org.springframework.data.redis.core.ListOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;
import java.util.stream.Collectors;

@RestController
public class SearchRegistController {
    @Resource
    public RedisTemplate redisTemplate;

    /**
     * postman提交的json报文：{"name":"provider-test","ip":"192.168.134.62","port":"8020"}
     *  raw ->json
     * @param serviceInfo
     * @return
     */
    @PostMapping("/regist")
    public String registServer(@RequestBody ServiceInfo serviceInfo) {
        System.out.println("打印日志:"+serviceInfo.toString());
        ListOperations<String,ServiceInfo> listOperations = redisTemplate.opsForList();
        //取出所有服务
        List<ServiceInfo> services = listOperations.range("services", 0, -1);
        System.out.println("services --->>"+services);
        //判断服务是否已经注册
        int size = services
                .stream()
                .filter(info -> info.equals(serviceInfo))
                .collect(Collectors.toList())
                .size();
        //size为0则将参数传进来的服务注册到redis
        if (size == 0) {
            listOperations.leftPush("services", serviceInfo);
            return "服务已经注册!";
        } else {
            return "服务存在,无需重复注册!";
        }
    }

    /**
     * 删除所有的service。测试用
     * @return
     */
    @GetMapping("/reset")
    public String delServices() {
        redisTemplate.delete("services");
        return "重置成功";
    }
}

