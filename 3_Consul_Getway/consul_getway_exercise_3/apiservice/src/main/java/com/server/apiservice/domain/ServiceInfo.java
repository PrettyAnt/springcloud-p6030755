package com.server.apiservice.domain;

import lombok.Data;

import java.io.Serializable;
@Data
public class ServiceInfo implements Serializable {
    public String name;
    public String ip;
    public String port;
    public boolean equals(ServiceInfo info){
        if (this.name.equals(info.getName()) &&
                this.ip.equals(info.getIp()) &&
                this.port.equals(info.getPort())) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString() {
        return "ServiceInfo{" +
                "name='" + name + '\'' +
                ", ip='" + ip + '\'' +
                ", port='" + port + '\'' +
                '}';
    }
}
