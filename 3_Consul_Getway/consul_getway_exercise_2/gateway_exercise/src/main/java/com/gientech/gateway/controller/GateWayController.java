package com.gientech.gateway.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import java.util.List;

@RestController
public class GateWayController {
    @Resource
    public RestTemplate restTemplate;
    @Resource
    public StringRedisTemplate stringRedisTemplate;
    @Autowired
    DiscoveryClient discoveryClient;
    /**
     * 设置参数接口，向redis中写入值
     * @return
     */
    @GetMapping("/setValue")
    public String setValue() {
        String serviceId = "consul-provider-exercise-01";
        List<ServiceInstance> instances = this.discoveryClient.getInstances(serviceId);
        String serviceValue = instances.get(0).getUri().toString();
        String interfaceValue = "goods";
        stringRedisTemplate.opsForValue().set("service",serviceValue);
        stringRedisTemplate.opsForValue().set("interface",interfaceValue);
        return "设置参数, service = "+instances.get(0).getUri()+", interface = "+interfaceValue;
    }
    @GetMapping("/{serviceName}/{interfaceName}")
    public String getway(@PathVariable("serviceName") String serviceName, @PathVariable("interfaceName") String interfaceName) {
        System.out.println("打印传入参数:  serviceName:"+serviceName+"  interfaceName:"+interfaceName);
        serviceName = stringRedisTemplate.opsForValue().get(serviceName);
        interfaceName=stringRedisTemplate.opsForValue().get(interfaceName);
        if (!StringUtils.hasText(serviceName)) {
            return "未找到该服务";
        }
        if (!StringUtils.hasText(interfaceName)) {
            return "接口名不能为空";
        }
        String url = serviceName + "/" + interfaceName;
        return restTemplate.getForObject(url, String.class);
    }


}
