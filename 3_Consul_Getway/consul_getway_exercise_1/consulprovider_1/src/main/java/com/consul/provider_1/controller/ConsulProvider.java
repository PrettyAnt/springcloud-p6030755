package com.consul.provider_1.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ConsulProvider {
    @GetMapping("/goods")
    public String makeCake() {
        return "produce . . .";
    }
}
