package com.consul.consumer_1.controller;

import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import java.util.List;

@RestController
public class ConsulConsumer {
    @Resource
    public RestTemplate restTemplate;
    @Resource
    public DiscoveryClient discoveryClient;

    @GetMapping("/find")
    public String find() {
        List<ServiceInstance> instances = discoveryClient.getInstances("consul-provider-exercise-01");
        ServiceInstance serviceInstance = instances.get(0);
        String url = serviceInstance.getUri()+"/goods";

        String template = restTemplate.getForObject(url, String.class);
        return "consume:"+template;
    }
}
