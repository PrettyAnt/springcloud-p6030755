package com.gientech.drools001.controller;

import com.gientech.drools001.entity.Order;
import org.kie.api.KieServices;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DroolsController {
    @GetMapping("/test")
    public String test() {
        //1、
        KieServices kieServices = KieServices.Factory.get();
        //2、
        KieContainer container = kieServices.getKieClasspathContainer();
        //3、
        KieSession kieSession = container.newKieSession();

        //实施对象
        Order order = new Order();
        order.setAmount(400);

        //4、
        kieSession.insert(order);
        //5、执行规则引擎
        kieSession.fireAllRules();

        //6、关闭keiSession
        kieSession.dispose();

        return " controller 执行了规则之后 = " + order.getScore();
    }
}
