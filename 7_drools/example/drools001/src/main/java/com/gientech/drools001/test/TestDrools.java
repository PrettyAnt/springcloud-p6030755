package com.gientech.drools001.test;

import com.gientech.drools001.entity.Customer;
import com.gientech.drools001.entity.Order;
import org.drools.core.base.RuleNameEqualsAgendaFilter;
import org.junit.Test;
import org.kie.api.KieServices;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;

import java.sql.Array;
import java.util.ArrayList;

public class TestDrools {
    /**
     * 基本测试
     */
    @Test
    public void test1() {
        //1、
        KieServices kieServices = KieServices.Factory.get();
        //2、
        KieContainer container = kieServices.getKieClasspathContainer();
        //3、
        KieSession kieSession = container.newKieSession();

        //实施对象
        Order order = new Order();
        order.setAmount(200);

        //4、
        kieSession.insert(order);
        //5、执行规则引擎
        kieSession.fireAllRules();

        //6、关闭keiSession
        kieSession.dispose();

        System.out.println("\n执行了规则之后 = " + order.getScore());
    }

    /**
     * 包含测试
     */
    @Test
    public void test2() {
        //1、
        KieServices kieServices = KieServices.Factory.get();
        //2、
        KieContainer container = kieServices.getKieClasspathContainer();
        //3、
        KieSession kieSession = container.newKieSession();

        //实施对象
        Order order = new Order();
        order.setAmount(200);
        Customer customer = new Customer();
        ArrayList<Order> orderList = new ArrayList<>();
        orderList.add(order);
        customer.setOrderList(orderList);

        //4、
        kieSession.insert(order);
        kieSession.insert(customer);
        //5、执行规则引擎
        kieSession.fireAllRules();

        //6、关闭keiSession
        kieSession.dispose();

    }

    /**
     * 不包含测试
     */
    @Test
    public void test3() {
        //1、
        KieServices kieServices = KieServices.Factory.get();
        //2、
        KieContainer container = kieServices.getKieClasspathContainer();
        //3、
        KieSession kieSession = container.newKieSession();

        //实施对象
        Order order = new Order();
        order.setAmount(200);
        Customer customer = new Customer();
        ArrayList<Order> orderList = new ArrayList<>();
//        orderList.add(order);
        customer.setOrderList(orderList);

        //4、
        kieSession.insert(order);
        kieSession.insert(customer);
        //5、执行规则引擎
        kieSession.fireAllRules();

        //6、关闭keiSession
        kieSession.dispose();

    }

    /**
     * 成员测试、过滤规则更改测试
     */
    @Test
    public void test4() {
        //1、
        KieServices kieServices = KieServices.Factory.get();
        //2、
        KieContainer container = kieServices.getKieClasspathContainer();
        //3、
        KieSession kieSession = container.newKieSession();

        //实施对象
        Order order = new Order();
        order.setAmount(200);
        Customer customer = new Customer();
        ArrayList<Order> orderList = new ArrayList<>();
        orderList.add(order);
        customer.setOrderList(orderList);

        //4、
        kieSession.insert(order);
        kieSession.insert(customer);
        //5、执行规则引擎
//        kieSession.fireAllRules();
        kieSession.fireAllRules(new RuleNameEqualsAgendaFilter("customer_1"));

        //6、关闭keiSession
        kieSession.dispose();

    }

    /**
     * 包含&成员对比测试
     */
    @Test
    public void test5() {
        //1、
        KieServices kieServices = KieServices.Factory.get();
        //2、
        KieContainer container = kieServices.getKieClasspathContainer();
        //3、
        KieSession kieSession = container.newKieSession();

        //实施对象
        Order order = new Order();
        order.setAmount(200);
        Customer customer = new Customer();
        ArrayList<Order> orderList = new ArrayList<>();
        orderList.add(order);
        customer.setOrderList(orderList);

        //4、
        kieSession.insert(order);
        kieSession.insert(customer);
        //5、执行规则引擎
        kieSession.fireAllRules();

        //6、关闭keiSession
        kieSession.dispose();

    }

    /**
     * matches 规则
     */
    @Test
    public void test6() {
        //1、
        KieServices kieServices = KieServices.Factory.get();
        //2、
        KieContainer container = kieServices.getKieClasspathContainer();
        //3、
        KieSession kieSession = container.newKieSession();

        //实施对象
        Customer customer = new Customer();
        customer.setName("王xx");

        //4、
        kieSession.insert(customer);
        //5、执行规则引擎
        kieSession.fireAllRules();

        //6、关闭keiSession
        kieSession.dispose();

    }


    /**
     * matches 规则
     */
    @Test
    public void test7() {
        //1、
        KieServices kieServices = KieServices.Factory.get();
        //2、
        KieContainer container = kieServices.getKieClasspathContainer();
        //3、
        KieSession kieSession = container.newKieSession();




        //5、执行规则引擎
        kieSession.fireAllRules();

        //6、关闭keiSession
        kieSession.dispose();

    }

    /**
     * matches 规则
     */
    @Test
    public void test8() {
        System.setProperty("drools.dateformat","yyyy-MM-dd");
        //1、
        KieServices kieServices = KieServices.Factory.get();
        //2、
        KieContainer container = kieServices.getKieClasspathContainer();
        //3、
        KieSession kieSession = container.newKieSession();

        //事实对象
        Customer customer = new Customer();
        customer.setName("李四");

        kieSession.insert(customer);

        //5、执行规则引擎
        kieSession.fireAllRules();

        //6、关闭keiSession
        kieSession.dispose();
        System.out.println("修改完成之后的客户是:"+customer.getName());
    }

    /**
     * 优先级测试
     */
    @Test
    public void test9() {
        System.setProperty("drools.dateformat","yyyy-MM-dd");
        //1、获取KieServices
        KieServices kieServices = KieServices.Factory.get();
        //2、获取KieContainer
        KieContainer container = kieServices.getKieClasspathContainer();
        //3、开启KieSessioon
        KieSession kieSession = container.newKieSession();

        //事实对象
        Customer customer = new Customer();
        customer.setName("李四");

        //Insert fact
        kieSession.insert(customer);

        //5、执行规则引擎
        kieSession.fireAllRules();

        //6、关闭keiSession
        kieSession.dispose();
    }
}
