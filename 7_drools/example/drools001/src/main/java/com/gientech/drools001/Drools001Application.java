package com.gientech.drools001;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Drools001Application {

    public static void main(String[] args) {
        SpringApplication.run(Drools001Application.class, args);
    }

}
