package com.gientech.drools001.entity;


import lombok.Data;

import java.util.List;

@Data
public class Customer {
    private String name;

    private Order order;

    private List<Order> orderList;
}
