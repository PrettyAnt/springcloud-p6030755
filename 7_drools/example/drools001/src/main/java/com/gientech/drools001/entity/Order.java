package com.gientech.drools001.entity;

import lombok.Data;

@Data
public class Order {
    //订单金额
    private int amount;
    //积分
    private int score;
}
