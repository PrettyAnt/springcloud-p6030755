package com.gientech.drools002;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Drools002Application {

    public static void main(String[] args) {
        SpringApplication.run(Drools002Application.class, args);
    }

}
