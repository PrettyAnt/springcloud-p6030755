package com.gientech.drools002.service;

import com.gientech.drools002.entity.Order;
import org.kie.api.KieBase;
import org.kie.api.runtime.KieSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RuleService {

    @Autowired
    private KieBase kieBase;

    public Order executeRule(Order order){
        KieSession kieSession = kieBase.newKieSession();
        //插入事实对象
        kieSession.insert(order);
        kieSession.fireAllRules();
        kieSession.dispose();

        return order;
    }
}
