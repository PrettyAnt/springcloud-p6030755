package com.gientech.drools002.controller;


import com.gientech.drools002.entity.Order;
import com.gientech.drools002.service.RuleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class OrderController {

    @Autowired
    private RuleService ruleService;

    @GetMapping("/matchOrder")
    public Order saveOrder(@RequestBody Order order){
        Order result = ruleService.executeRule(order);
        //使用postman测试，需要在head里，将Content-Type 设置为 application/json
        return result;
    }
}
