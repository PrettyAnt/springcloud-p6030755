package com.drools.queryprice;

import com.drools.queryprice.entity.CelebrateDate;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.StringUtils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@SpringBootTest
class QueryPriceApplicationTests {

    @Test
    void contextLoads() {
//        System.out.println("result = " + monthPreiod("0", "2021-08-11"));
        System.out.println("result = " + celDate("2023-06-17"));
    }

    /**
     * @param nearOrderTime 上一次下单时间
     * @param orderTime     下单时间
     * @return
     */
    public int monthPreiod(String nearOrderTime, String orderTime) {
        if (!StringUtils.hasLength(nearOrderTime) || nearOrderTime.equals("0")) {
            return 999;
        }
        int result = 0;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar c1 = Calendar.getInstance();
        Calendar c2 = Calendar.getInstance();
        try {
            c1.setTime(sdf.parse(nearOrderTime));
            c2.setTime(sdf.parse(orderTime));
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
        result = c2.get(Calendar.MONTH) - c1.get(Calendar.MONTH) + (c2.get(Calendar.YEAR) - c1.get(Calendar.YEAR)) * 12;
        return Math.abs(result);
    }

    public static int celDate(String orderTime) {
        String year = orderTime.substring(0, orderTime.indexOf("-"));
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        try {
            //下单日期
            Date orderDate = sdf.parse(orderTime);
            CelebrateDate[] values = CelebrateDate.values();
            for (CelebrateDate value : values) {
                //特殊节日日期
                Date nearDate = sdf.parse(year+"-"+value.getDate());
                int days = (int) ((nearDate.getTime() - orderDate.getTime()) / (1000 * 60 * 60 * 24));
                if (days <= 10 && days >= 0) {
                    return value.getId();
                }
            }
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
        return -1;
    }

}
