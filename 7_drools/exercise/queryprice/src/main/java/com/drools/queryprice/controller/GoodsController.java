package com.drools.queryprice.controller;


import com.drools.queryprice.entity.Goods;
import com.drools.queryprice.service.RuleService;
import com.drools.queryprice.util.DroolsUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
public class GoodsController {

    @Autowired
    private RuleService ruleService;
    @Resource
    public RedisTemplate redisTemplate;

    /**
     * 获取商品价格接口
     *
     * @param goods
     * @return
     */
    @PostMapping("/queryPrice")
    public Goods queryPrice(@RequestBody Goods goods) {
        String userId = goods.getUserId();
        //是否为618 1111 活动
        goods.setCelebrateDate(DroolsUtils.celDate(goods.getOrderTime()));
        //查询redis,获取年度消费总额、最近下单时间
        Goods redisGoods = (Goods) redisTemplate.opsForValue().get(userId);
        if (redisGoods == null) {
            //新用户间隔时间为0，即不享受优惠
            goods.setNewUser(true);
            return ruleService.executeNewUserRule(goods);
        } else {
            goods.setNewUser(false);
            //设置消费间隔
            goods.setPreiodTime(DroolsUtils.monthPreiod(redisGoods.getNearOrderTime(), goods.getOrderTime()));
            //设置年度消费总额
            goods.setAmount(redisGoods.getAmount());
        }
        Goods result = ruleService.executeRule(goods);
        result.setDiscount(Math.round(result.getDiscount() * 100) / 100f);
        //使用postman测试，需要在head里，将Content-Type 设置为 application/json
        return result;
    }

    /**
     * 下单接口
     *
     * @param goods
     * @return
     */
    @PostMapping("/placeOrder")
    public Goods placeOrder(@RequestBody Goods goods) {
        String userId = goods.getUserId();
        ValueOperations ops = redisTemplate.opsForValue();
        Goods redisUserInfo = (Goods) ops.get(userId);
        if (redisUserInfo == null) {
            goods.setNewUser(false);
            ops.set(userId, goods);
        } else {
            boolean currentYearOrder = DroolsUtils.isCurrentYearOrder(goods.getOrderTime());
            if (currentYearOrder) {
                //更新年度订单总额
                redisUserInfo.setAmount(redisUserInfo.getAmount() + goods.getShowPrice());
            } else {
                redisUserInfo.setAmount(goods.getShowPrice());
            }
            redisUserInfo.setUserId(goods.getUserId());
            //更新最近一次的下单时间
            redisUserInfo.setNearOrderTime(goods.getOrderTime());
            redisUserInfo.setOrderTime(goods.getOrderTime());
            redisUserInfo.setNewUser(false);
            ops.set(userId, redisUserInfo);
        }
        System.out.println("redisUserInfo -------> " + ops.get(userId) == null ? "" : ops.get(userId).toString());
        return redisUserInfo;
    }

}
