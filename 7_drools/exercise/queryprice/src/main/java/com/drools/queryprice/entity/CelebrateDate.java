package com.drools.queryprice.entity;

public enum CelebrateDate {
    cel_618(618,"06-18"),cel_1111(1111, "11-11");

    private  int id;
    private  String date;

    CelebrateDate(int id, String date) {
        this.id = id;
        this.date = date;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
