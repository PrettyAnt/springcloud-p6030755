package com.drools.queryprice.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class Goods implements Serializable {

    private static final long serialVersionUID = 1447315490361028900L;
    private String userId;//用户编号

    private float realPrice; //订单实际金额
    private float showPrice; //订单显示给用户的金额
    private String nearOrderTime; //上一次的下单时间
    private String orderTime; //下单时间
    private boolean vip=true;//是否为vip用户
    private int preiodTime;//下单间隔
    private String celebrateDate;//活动日

    private float discount;//折扣

    private float amount;//消费总额

    private boolean newUser;//是否为新用户


}
