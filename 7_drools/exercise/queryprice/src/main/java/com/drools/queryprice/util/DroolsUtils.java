package com.drools.queryprice.util;

import com.drools.queryprice.entity.CelebrateDate;
import org.springframework.util.StringUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DroolsUtils {

    /**
     * @param nearOrderTime 上一次下单时间
     * @param orderTime     下单时间
     * @return
     */
    public static int monthPreiod(String nearOrderTime, String orderTime) {
        if (!StringUtils.hasLength(nearOrderTime) || nearOrderTime.equals("0")) {
            return 999;
        }
        int result = 0;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar c1 = Calendar.getInstance();
        Calendar c2 = Calendar.getInstance();
        try {
            Date nearDate = sdf.parse(nearOrderTime);
            Date date = sdf.parse(orderTime);
            int days = (int) ((date.getTime() - nearDate.getTime()) / (1000 * 60 * 60 * 24));
            if (days < 30) {
                return 0;
            }
            c1.setTime(nearDate);
            c2.setTime(date);
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
        result = c2.get(Calendar.MONTH) - c1.get(Calendar.MONTH) + (c2.get(Calendar.YEAR) - c1.get(Calendar.YEAR)) * 12;
        return Math.abs(result);
    }

    /**
     * 判断当前日期是否为特殊节日
     *
     * @param orderTime
     * @return
     */
    public static String celDate(String orderTime) {
        String year = orderTime.substring(0, orderTime.indexOf("-"));
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        try {
            //下单日期
            Date orderDate = sdf.parse(orderTime);
            CelebrateDate[] values = CelebrateDate.values();
            for (CelebrateDate value : values) {
                //特殊节日日期
                Date nearDate = sdf.parse(year + "-" + value.getDate());
                int days = (int) ((nearDate.getTime() - orderDate.getTime()) / (1000 * 60 * 60 * 24));
                if (days <= 10 && days >= 0) {
                    return String.valueOf(value.getId());
                }
            }
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
        return "无";
    }

    /**
     * 是否为当年的订单
     *
     * @return
     */
    public static boolean isCurrentYearOrder(String orderTime) {
        SimpleDateFormat sdf= new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date(System.currentTimeMillis());
        String currentYear = sdf.format(date);
        currentYear = currentYear.substring(0, currentYear.indexOf("-"));
        orderTime = orderTime.substring(0, orderTime.indexOf("-"));
        if (currentYear.equals(orderTime)) {
            return true;
        }
        return false;
    }
}
