package com.drools.queryprice.service;

import com.drools.queryprice.entity.Goods;
import org.drools.core.base.RuleNameEqualsAgendaFilter;
import org.drools.core.base.RuleNameStartsWithAgendaFilter;
import org.kie.api.KieBase;
import org.kie.api.runtime.KieSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RuleService {

    @Autowired
    private KieBase kieBase;

    public Goods executeRule(Goods goods){
        KieSession kieSession = kieBase.newKieSession();
        //插入事实对象
        kieSession.insert(goods);
        kieSession.fireAllRules();
        kieSession.dispose();

        return goods;
    }

    public Goods executeNewUserRule(Goods goods){
        KieSession kieSession = kieBase.newKieSession();
        //插入事实对象
        kieSession.insert(goods);
        //触发新用户
        kieSession.fireAllRules(new RuleNameStartsWithAgendaFilter("newUser"));
//        kieSession.fireAllRules();
        kieSession.dispose();
        return goods;
    }
}
