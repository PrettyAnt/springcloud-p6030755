package com.drools.queryprice.util;

import com.drools.queryprice.entity.Response;
import com.google.gson.Gson;

public class ResponseUtil {
    public static String returnData(int code, String message) {
        Gson gson = new Gson();
        Response response = new Response();
        response.setCode(code);
        response.setMessage(message);
        String result = gson.toJson(response);
        System.out.println("发出去的报文:" + result);
        return result;
    }
}
