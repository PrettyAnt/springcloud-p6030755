package com.gientech.flowable001.actions;

import org.flowable.engine.delegate.DelegateExecution;
import org.flowable.engine.delegate.JavaDelegate;

public class CallExternalSystemDelegate implements JavaDelegate {
    /**
     * 这个一个Flowable中的触发器
     * @param execution
     */
    @Override
    public void execute(DelegateExecution execution) {
        // 触发执行的逻辑  在外部系统中安排相应假期
        System.out.println("访问外部系统，进行相应假期安排。。。。。");
    }
}
