package com.gientech.flowable001;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Flowable001Application {

    public static void main(String[] args) {
        SpringApplication.run(Flowable001Application.class, args);
    }

}
