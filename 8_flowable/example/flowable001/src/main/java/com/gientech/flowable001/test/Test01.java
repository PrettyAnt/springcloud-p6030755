package com.gientech.flowable001.test;

import org.flowable.engine.*;
import org.flowable.engine.history.HistoricActivityInstance;
import org.flowable.engine.impl.cfg.StandaloneProcessEngineConfiguration;
import org.flowable.engine.repository.Deployment;
import org.flowable.engine.repository.ProcessDefinition;
import org.flowable.engine.runtime.ProcessInstance;
import org.flowable.task.api.Task;
import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Test01 {

    /**
     * 获取流程引擎对象
     */
    @Test
    public void testProcessEngine() {
        // 获取 ProcessEngineConfiguration 对象
        ProcessEngineConfiguration configuration = new StandaloneProcessEngineConfiguration();
        // 配置 相关的数据库连接信息
        configuration.setJdbcDriver("com.mysql.cj.jdbc.Driver");
        configuration.setJdbcUsername("root");
        configuration.setJdbcPassword("123456");
        // UTC是统一标准世界时间
        //也可以设置以下时区
        //北京时间东八区
        //serverTimezone=GMT%2B8
        //上海时间
        //serverTimezone=Asia/Shanghai
        configuration.setJdbcUrl("jdbc:mysql://localhost:3306/flowable-learn?serverTimezone=UTC");
        // 如果数据库中的表结构不存在就新建
        configuration.setDatabaseSchemaUpdate(ProcessEngineConfiguration.DB_SCHEMA_UPDATE_TRUE);

        // 通过 ProcessEngineConfiguration 构建 processEngine 对象
        ProcessEngine processEngine = configuration.buildProcessEngine();
    }

    /*
        数据库表名说明：

        Flowable的所有数据库表都以ACT_开头。第二部分是说明表用途的两字符标示符。服务API的命名也大略符合这个规则。

        ACT_RE_*: 'RE’代表repository。带有这个前缀的表包含“静态”信息，例如流程定义与流程资源（图片、规则等）。

        ACT_RU_*: 'RU’代表runtime。这些表存储运行时信息，例如流程实例（process instance）、用户任务（user task）、变量（variable）、作业（job）等。Flowable只在流程实例运行中保存运行时数据，并在流程实例结束时删除记录。这样保证运行时表小和快。

        ACT_HI_*: 'HI’代表history。这些表存储历史数据，例如已完成的流程实例、变量、任务等。

        ACT_GE_*: 通用数据。在多处使用。
    */

    ProcessEngineConfiguration configuration = null;

    @Before
    // import org.junit.Before; 在一个类中最先执行的方法
    public void before(){
        // 获取 ProcessEngineConfiguration 对象
        configuration = new StandaloneProcessEngineConfiguration();
        // 配置 相关的数据库连接信息
        configuration.setJdbcDriver("com.mysql.cj.jdbc.Driver");
        configuration.setJdbcUsername("root");
        configuration.setJdbcPassword("123456");
        // UTC是统一标准世界时间
        configuration.setJdbcUrl("jdbc:mysql://localhost:3306/flowable-learn?serverTimezone=UTC");
        // 如果数据库中的表结构不存在就新建
        configuration.setDatabaseSchemaUpdate(ProcessEngineConfiguration.DB_SCHEMA_UPDATE_TRUE);
    }

    /**
     * 部署流程
     */
    @Test
    public void testDeploy(){
        // 1、获取 ProcessEngine 对象
        ProcessEngine processEngine = configuration.buildProcessEngine();
        // 2、获取RepositoryService  资源相关的服务
        RepositoryService repositoryService = processEngine.getRepositoryService();
        // 3、完成流程的部署操作
        Deployment deploy = repositoryService.createDeployment()
                .addClasspathResource("holiday-request.bpmn20.xml") // 关联要部署的流程文件
                .name("请假流程")
                .deploy(); // 部署流程
        System.out.println("deploy.getId() = " + deploy.getId() + " , this is deployment id as used in following.");
        System.out.println("deploy.getName() = " + deploy.getName());

        //关注两张数据表: ACT_RE_DEPLOYMENT(发布信息表) , ACT_RE_PROCDEF(流程定义表), ACT_GE_BYTEARRAY
    }

    /**
     * 查询流程定义的信息
     */
    @Test
    public void testDeployQuery() {
        ProcessEngine processEngine = configuration.buildProcessEngine();
        RepositoryService repositoryService = processEngine.getRepositoryService();

        // 查询单个
        // 如果 查询多个， 就用 deploymentIds
        ProcessDefinition processDefinition = repositoryService.createProcessDefinitionQuery().deploymentId("172501").singleResult();
        System.out.println("processDefinition.getName() = " + processDefinition.getName());
        System.out.println("processDefinition.getId() = " + processDefinition.getId());
        System.out.println("processDefinition.getDeploymentId() = " + processDefinition.getDeploymentId());
        System.out.println("processDefinition.getDescription() = " + processDefinition.getDescription());
    }

    /**
     * 删除流程定义
     */
    @Test
    public void testDeleteDeploy() {
        ProcessEngine processEngine = configuration.buildProcessEngine();
        RepositoryService repositoryService = processEngine.getRepositoryService();
        // 删除部署的流程 第一个参数id，如果部署的流程启动了就不允许删除
//        repositoryService.deleteDeployment("1");

        // 第二个参数是级联删除，如果部署的流程启动了 相关的任务会一并被删除
        repositoryService.deleteDeployment("172501", true);
    }

    /**
     * 启动流程实例
     */
    @Test
    public void testRunProcess() {
        ProcessEngine processEngine = configuration.buildProcessEngine();
        // 通过RuntimeService启动流程实例
        RuntimeService runtimeService = processEngine.getRuntimeService();

        // 构建流程变量
        Map<String, Object> variables = new HashMap<String, Object>();
        variables.put("employee", "user1");
        variables.put("days", 3);
        variables.put("description", "user1的描述信息");


        // 流程key <process id="holidayRequest" name="Holiday Request" isExecutable="true">
        ProcessInstance holidayRequest = runtimeService.startProcessInstanceByKey("holidayRequest", variables);
        System.out.println("holidayRequest.getProcessDefinitionId() = " + holidayRequest.getProcessDefinitionId());
        System.out.println("holidayRequest.getId() = " + holidayRequest.getId());
        System.out.println("holidayRequest.getActivityId() = " + holidayRequest.getActivityId());
        System.out.println("holidayRequest.getName() = " + holidayRequest.getName());

        System.out.println("流程定义的ID：" + holidayRequest.getProcessDefinitionId());
        System.out.println("流程实例的ID：" + holidayRequest.getId());
        System.out.println("当前活动的ID：" + holidayRequest.getActivityId());
        System.out.println("活动的名称" + holidayRequest.getName());


        // act_ru_variable、 act_ru_task、act_ru_execution 表可查看到相关信息。
    }

    //查询当前用户下的流程
    @Test
    public void testQueryTaskForLisi(){
        String currentUsername = "lisi";
        // 获取流程引擎对象
        ProcessEngine processEngine = configuration.buildProcessEngine();
        TaskService taskService = processEngine.getTaskService();
        List<Task> list = taskService.createTaskQuery()
                .processDefinitionKey("holidayRequest")
                .taskAssignee(currentUsername)
                .list();
        for (Task task : list) {
            System.out.println("=================================================");
            System.out.println("task.getProcessDefinitionId() = " + task.getProcessDefinitionId());
            System.out.println("task.getId() = " + task.getId());
            System.out.println("task.getAssignee() = " + task.getAssignee());
            System.out.println("task.getName() = " + task.getName());
        }
    }

    /**
     * 完成当前任务: 不批假期
     */
    @Test
    public void testCompleteTaskRejected(){
        ProcessEngine processEngine = configuration.buildProcessEngine();
        TaskService taskService = processEngine.getTaskService();
        Task task = taskService.createTaskQuery()
                .processDefinitionKey("holidayRequest")
                .taskAssignee("lisi")
                .singleResult();
        // 创建流程变量
        Map<String,Object> map = new HashMap<>();
        map.put("approved",false);
        // 完成任务
        taskService.complete(task.getId(),map);

    }

    /**
     * 完成当前任务: 批准假期
     */
    @Test
    public void testCompleteTaskApproved(){
        ProcessEngine processEngine = configuration.buildProcessEngine();
        TaskService taskService = processEngine.getTaskService();
        Task task = taskService.createTaskQuery()
                .processDefinitionKey("holidayRequest")
                .taskAssignee("wangwu")
                .singleResult();

        System.out.println("=====================================");
        // 创建流程变量
        Map<String,Object> map = new HashMap<>();
        map.put("approved",true);
        // 完成任务
        taskService.complete(task.getId(),map);

    }

    /**
     * 完成当前任务: 批准假期
     */
    @Test
    public void testCompleteTaskHolidayApproved(){
        ProcessEngine processEngine = configuration.buildProcessEngine();
        TaskService taskService = processEngine.getTaskService();
        Task task = taskService.createTaskQuery()
                .processDefinitionKey("holidayRequest")
                .taskAssignee("wangwu")
                .singleResult();
        // 创建流程变量
        Map<String,Object> map = new HashMap<>();
        map.put("approved",true);
        // 完成任务
        taskService.complete(task.getId(),map);
    }


    /**
     * 获取流程任务的历史数据
     */
    @Test
    public void testHistory(){
        ProcessEngine processEngine = configuration.buildProcessEngine();
        HistoryService historyService = processEngine.getHistoryService();
        List<HistoricActivityInstance> list = historyService.createHistoricActivityInstanceQuery()
                .processDefinitionId("holidayRequest:1:172503")
                .finished() // 查询的历史记录的状态是已经完成
                .orderByHistoricActivityInstanceEndTime().asc() // 指定排序的字段和顺序
                .list();
        for (HistoricActivityInstance history : list) {
            System.out.println(history.getActivityName()+":"+history.getAssignee()+"--"
                    +history.getActivityId()+":" + history.getDurationInMillis()+"毫秒");
        }

    }
}
