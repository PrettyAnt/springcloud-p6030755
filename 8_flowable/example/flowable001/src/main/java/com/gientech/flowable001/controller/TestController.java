package com.gientech.flowable001.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;

@RestController
public class TestController {

    @GetMapping("/test")
    public String test(){
        return "test success";
    }
    //简单的用户登录
    @GetMapping("/setSession")
    public String setSession(HttpSession session, @RequestParam(value = "username") String username){
        //session.setAttribute("uid","123456");
        String sessionId = session.getId();
        System.out.println("username : " + username);
        System.out.println("sessionId : " + sessionId);
        session.setAttribute("username", username);
        return "succ";
    }

    @GetMapping("/getSession")
    public String  getSession(HttpSession session){
        String username = session.getAttribute("username").toString();
        return username;
    }
}
