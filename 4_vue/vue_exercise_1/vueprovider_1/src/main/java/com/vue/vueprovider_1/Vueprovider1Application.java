package com.vue.vueprovider_1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Vueprovider1Application {

	public static void main(String[] args) {
		SpringApplication.run(Vueprovider1Application.class, args);
	}

}
