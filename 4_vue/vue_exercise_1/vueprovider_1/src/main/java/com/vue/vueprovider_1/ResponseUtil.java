package com.vue.vueprovider_1;

import com.google.gson.Gson;
import com.vue.vueprovider_1.bean.Response;

public class ResponseUtil {
    public static String returnData(int code, String message) {
        Gson gson = new Gson();
        Response response = new Response();
        response.setCode(code);
        response.setMessage(message);
        String result = gson.toJson(response);
        System.out.println("发出去的报文:" + result);
        return result;
    }
}
