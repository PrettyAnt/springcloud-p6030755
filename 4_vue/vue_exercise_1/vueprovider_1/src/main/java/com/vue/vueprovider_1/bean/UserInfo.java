package com.vue.vueprovider_1.bean;

import java.util.List;

public class UserInfo {

    /**
     * username : 卢本伟
     * password : 123456
     * certainPassword : 123456
     * date : 2023-03-23
     * idType : 党员
     * edu : 博士
     * github : prettyant.com
     * hobby : ["篮球"]
     */

    private String       username;
    private String       userId;
    private String       password;
    private String       certainPassword;
    private String       date;
    private String       idType;
    private String       edu;
    private String       github;
    private List<String> hobby;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getCertainPassword() {
        return certainPassword;
    }

    public void setCertainPassword(String certainPassword) {
        this.certainPassword = certainPassword;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getIdType() {
        return idType;
    }

    public void setIdType(String idType) {
        this.idType = idType;
    }

    public String getEdu() {
        return edu;
    }

    public void setEdu(String edu) {
        this.edu = edu;
    }

    public String getGithub() {
        return github;
    }

    public void setGithub(String github) {
        this.github = github;
    }

    public List<String> getHobby() {
        return hobby;
    }

    public void setHobby(List<String> hobby) {
        this.hobby = hobby;
    }

    @Override
    public String toString() {
        return "UserInfo{" +
                "username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", certainPassword='" + certainPassword + '\'' +
                ", date='" + date + '\'' +
                ", idType='" + idType + '\'' +
                ", edu='" + edu + '\'' +
                ", github='" + github + '\'' +
                ", hobby=" + hobby +
                '}';
    }
}
