package com.vue.vueprovider_1.controller;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.vue.vueprovider_1.ResponseUtil;
import com.vue.vueprovider_1.bean.Response;
import com.vue.vueprovider_1.bean.UserInfo;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Set;

@RestController
public class VueProviderController {
    @Resource
    public StringRedisTemplate stringRedisTemplate;

    @GetMapping(value = "/test",produces = "text/html;charset=UTF-8")
    public String test() {
        return "test - 测试是否乱码";
    }

    @PostMapping("/regist")
    public String regist(HttpServletRequest httpServletRequest) {
        try {
            String data = charReader(httpServletRequest);
            System.out.println("接收报文:" + data);
            Gson gson = new Gson();
            UserInfo userInfo = gson.fromJson(data, UserInfo.class);
            String username = userInfo.getUsername();
            String result = stringRedisTemplate.opsForValue().get(username);
            if (StringUtils.hasLength(result)) {
                return ResponseUtil.returnData(1, "请勿重复注册,为您跳转到查询页面!");
            }
            //存入redis中
            stringRedisTemplate.opsForValue().set(username, data);
        } catch (IOException e) {
            return ResponseUtil.returnData(-1, "转码异常");
        } catch (JsonSyntaxException e) {
            return ResponseUtil.returnData(-1, "json解析异常");
        }
        return ResponseUtil.returnData(0, "注册成功");
    }


    //字符串读取
    static String charReader(HttpServletRequest request) throws IOException {
        BufferedReader br = request.getReader();
        String str, wholeStr = "";
        while ((str = br.readLine()) != null) {
            wholeStr += str;
        }
        return wholeStr;
    }

    @GetMapping("/query")
    public String query(@RequestParam("userName") String userName) {
        System.out.println("查询的key为:" + userName);
        String data = stringRedisTemplate.opsForValue().get(userName);
        System.out.println("查询的结果为:" + data);
        if (StringUtils.hasLength(data)) {
            return ResponseUtil.returnData(0, data);
        } else {
            return ResponseUtil.returnData(-1, "查询失败");
        }
    }

    @GetMapping("/reset")
    public String reset() {
        Set<String> keys = stringRedisTemplate.keys("*");
        for (String key : keys) {
            stringRedisTemplate.delete(key);
        }
        return "dele-success";
    }
}
