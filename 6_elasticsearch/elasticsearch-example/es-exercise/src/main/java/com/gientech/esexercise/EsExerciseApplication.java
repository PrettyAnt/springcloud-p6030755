package com.gientech.esexercise;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EsExerciseApplication {

    public static void main(String[] args) {
        SpringApplication.run(EsExerciseApplication.class, args);
    }

}
