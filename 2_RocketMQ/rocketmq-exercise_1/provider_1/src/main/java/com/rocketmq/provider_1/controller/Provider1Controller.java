package com.rocketmq.provider_1.controller;

import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
public class Provider1Controller {
    @Resource
    private RocketMQTemplate rocketMQTemplate;

    @RequestMapping("/send")
    public String sendMessage() {
        rocketMQTemplate.convertAndSend("prettyant","sendMessage ... ");
        return "发送了一条消息";
    }
}
