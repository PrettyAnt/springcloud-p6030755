package com.rocketmq.consumer_1.controller;

import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.stereotype.Component;

@Component
@RocketMQMessageListener(topic = "topic_loan", consumerGroup = "prettyant-loan-group")
public class Consumer1Controller implements RocketMQListener<String> {

    @Override
    public void onMessage(String message) {
        System.out.println("收到了一条消息---->>>" + message);
    }
}
