package com.rocketmq.provider_2.controller;

import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
public class Provider2Controller {
    @Resource
    private RocketMQTemplate rocketMQTemplate;

    @RequestMapping("/regist")
    private String regist() {
        //用户信息
        String user1 = "name:prettyant,pwd:123456,data:技术老师真的帅";
        //发送消息到rocketmq,主题为 topic1,内容为 user1
        rocketMQTemplate.convertAndSend("topic1", user1);
        return "注册信息";
    }
}
