package com.rocketmq.consumer_2.controller;

import org.apache.commons.lang3.StringUtils;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RocketMQMessageListener(topic = "topic1",consumerGroup = "prettyant-consumer-group")
public class Consumer2Controller implements RocketMQListener<String> {
    @Resource
    RedisTemplate<String, String> redisTemplate;
    private String key="user2";

    @Override
    public void onMessage(String s) {
        //收到订阅消息
        System.out.println("Receive a new Message:" + s);
        if (!StringUtils.isEmpty(s)) {
            //将消息存到redis中
            redisTemplate.opsForValue().set(key,s);
        }
    }

    @GetMapping("/checkuser")
    public String checkUser() {
        //从redis中获取消息(查询消息)
        String value = redisTemplate.opsForValue().get(key);
        if (!StringUtils.isEmpty(value)) {
            //将消息展示在页面上
            return "user info is :" + value;
        }
        return "user unregist~";
    }
}
