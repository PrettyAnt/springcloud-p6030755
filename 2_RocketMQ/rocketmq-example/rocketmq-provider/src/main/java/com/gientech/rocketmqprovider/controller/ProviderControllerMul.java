package com.gientech.rocketmqprovider.controller;


import org.apache.rocketmq.client.exception.MQBrokerException;
import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.client.producer.SendCallback;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.common.CountDownLatch2;
import org.apache.rocketmq.common.message.Message;
import org.apache.rocketmq.remoting.exception.RemotingException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.TimeUnit;

@RestController
public class ProviderControllerMul {
    @GetMapping("/sync")
    public void syncMethod() throws MQClientException, MQBrokerException, RemotingException, InterruptedException, IOException {
        //实例化消息生产者Producer
        DefaultMQProducer producer = new DefaultMQProducer("please_rename_unique_group_name");
        //设置NameServer的地址
        producer.setNamesrvAddr("localhost:9876");
        //启动Producer实例
        producer.start();
        producer.setRetryTimesWhenSendAsyncFailed(0);
        int messageCount = 100;
        CountDownLatch2 countDownLatch = new CountDownLatch2(messageCount);
        for (int i = 0; i < 10; i++) {
            final int index = i;
            Message msg = new Message("PrettyAnt", "Tag1", ("Hello RocketMQ  [" + i + "]").getBytes(StandardCharsets.UTF_8));
            //发送消息到一个Broker
            //sendCallBack接受接收异步返回结果的回调
            producer.send(msg, new SendCallback() {
                @Override
                public void onSuccess(SendResult sendResult) {
                    System.out.printf("%-10d 0k %s %n", index, sendResult.getMsgId());
                }

                @Override
                public void onException(Throwable throwable) {
                    System.out.printf("%-10d Exception %s %n", index, throwable);
                    throwable.printStackTrace();
                }
            });

        }
        countDownLatch.await(5, TimeUnit.SECONDS);
        //如果不再发送消息，关闭Producer实例
        producer.shutdown();
    }

    @GetMapping("/single")
    public void single() throws MQClientException, RemotingException, InterruptedException {
        //实例化消息生产者Producer
        DefaultMQProducer producer = new DefaultMQProducer("please_rename_unique_group_name");
        //设置NameServer的地址
        producer.setNamesrvAddr("localhost:9876");
        //启动Producer实例
        producer.start();
        for (int i = 0; i < 10; i++) {
            Message msg = new Message("PrettyAnt", "Tag1", ("Hello RocketMQ  [" + i + "]").getBytes(StandardCharsets.UTF_8));
            //发送消息到一个Broker
//            SendResult sendResult = producer.send(msg);
            //发送单项消息，没有任何返回结果
            producer.sendOneway(msg);
        }
        //如果不再发送消息，关闭Producer实例
        producer.shutdown();
    }

}
