package com.gientech.rocketmqprovider.controller;


import org.apache.rocketmq.client.exception.MQBrokerException;
import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.common.message.Message;
import org.apache.rocketmq.remoting.exception.RemotingException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

@RestController
public class ProviderController {
    @GetMapping("/syncMethod")
    public void syncMethod() throws MQClientException, MQBrokerException, RemotingException, InterruptedException, IOException {
        //实例化消息生产者Producer
        DefaultMQProducer producer = new DefaultMQProducer("provider");
        //设置NameServer的地址
        producer.setNamesrvAddr("localhost:9876");
        //启动Producer实例
        producer.start();
        for (int i = 0; i < 10; i++) {
            Message msg = new Message("PrettyAnt", "Tag1", ("Hello RocketMQ  [" + i+"]").getBytes(StandardCharsets.UTF_8));
            //发送消息到一个Broker
            SendResult sendResult = producer.send(msg);
            //通过sentResult返回消息是否成功发送
            System.out.printf("%s%n",sendResult);

        }
        //如果不再发送消息，关闭Producer实例
//        System.in.read();
        producer.shutdown();
    }

}
