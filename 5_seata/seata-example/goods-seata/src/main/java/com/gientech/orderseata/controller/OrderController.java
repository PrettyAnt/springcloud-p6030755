package com.gientech.orderseata.controller;

import com.gientech.orderseata.dao.OrderDao;
import com.gientech.orderseata.pojo.Order;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class OrderController {
    @GetMapping("/test")
    public String test() {
        System.out.println("-----success-----------");
        return "success";
    }

    @Autowired(required = true)
    private OrderDao orderDao;

    @Autowired
    public RestTemplate restTemplate;

    @GetMapping("/addOrder")
    @GlobalTransactional
    public String addOrder(){
        System.out.println("-------------addOrder start---------");
        Order o = new Order();
        o.setProductId(666888);
        o.setTotalAmount(80);
        o.setStatus(1);


        //1：成功  0: 失败
        int rst = orderDao.addOrder(o);
        System.out.println("rst : " + rst);

//        String msg = restTemplate.getForObject("http://localhost:8040/addStock" , String.class);
//
//        System.out.println("msg : " + msg);

        return "add Order Success";
    }
}
