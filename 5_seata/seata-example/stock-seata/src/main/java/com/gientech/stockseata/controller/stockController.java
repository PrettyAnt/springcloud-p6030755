package com.gientech.stockseata.controller;

import com.gientech.stockseata.dao.StockDao;
import com.gientech.stockseata.pojo.Stock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class stockController {
    @Autowired
    private StockDao stockDao;

    @GetMapping("/addStock")
    public String addOrder() {

        Stock s = new Stock();
        s.setProductId(12345);
        s.setCount(99);

        int rst = stockDao.addStock(s);

        System.out.println("rst : " + rst);

        return "";
    }
}
