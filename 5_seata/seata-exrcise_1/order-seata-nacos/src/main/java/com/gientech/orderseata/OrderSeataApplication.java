package com.gientech.orderseata;

import io.seata.spring.annotation.datasource.EnableAutoDataSourceProxy;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Bean;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
@EnableAutoDataSourceProxy
public class OrderSeataApplication {

	public static void main(String[] args) {
		SpringApplication.run(OrderSeataApplication.class, args);
	}

	@Bean
	public RestTemplate restTemplate() { return new RestTemplate();}
}
