package com.gientech.orderseata.controller;

import com.gientech.orderseata.dao.OrderDao;
import com.gientech.orderseata.pojo.Order;
import io.seata.spring.annotation.GlobalLock;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class orderController {

    @Autowired
    private OrderDao orderDao;

    @Autowired
    public RestTemplate restTemplate;

    @GetMapping("/addOrder")
    @GlobalTransactional
    public String addOrder(){

        Order o = new Order();
        o.setProductId(12345);
        o.setTotalAmount(100);
        o.setStatus(1);


        int rst = orderDao.addOrder(o);
        System.out.println("rst : " + rst);

        String msg = restTemplate.getForObject("http://localhost:8010/addStock" , String.class);

        System.out.println("msg : " + msg);

        int a = 1/0;

        return msg;
    }

}
