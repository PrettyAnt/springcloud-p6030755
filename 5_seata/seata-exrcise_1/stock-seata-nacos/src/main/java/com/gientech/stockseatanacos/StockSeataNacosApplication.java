package com.gientech.stockseatanacos;

import io.seata.spring.annotation.datasource.EnableAutoDataSourceProxy;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@EnableAutoDataSourceProxy
public class StockSeataNacosApplication {

	public static void main(String[] args) {
		SpringApplication.run(StockSeataNacosApplication.class, args);
	}

}
