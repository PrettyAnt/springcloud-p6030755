package com.gientech.stockseatanacos.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Stock {

    private Integer id;

    private Integer productId;

    private Integer count;
}
