package com.gientech.stockseatanacos.dao;


import com.gientech.stockseatanacos.pojo.Stock;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Mapper  // 告诉springboot这是一个mybatis的mapper
@Repository // 将OrderDao交由spring容器管理
public interface StockDao {

    public int addStock(Stock stock);

}
